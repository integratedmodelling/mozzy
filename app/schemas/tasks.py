from pydantic import BaseModel, Field
from uuid import UUID
from typing import Optional, List
from enum import Enum

class TaskState(str, Enum):
    running = 'running'
    error = 'error'
    finished = 'finished'
    waiting = 'waiting'

class SubTaskSchema(BaseModel):
    id: Optional[int]
    subtask_id: str
    state: TaskState =  Field(None, alias='state')
    parent_id: UUID
    class Config:
        orm_mode = True


class UpdateSubTaskSchema(SubTaskSchema):
    state: TaskState =  Field(None, alias='state')


class TaskSchema(BaseModel):
    id: Optional[UUID]
    state: TaskState =  Field(None, alias='state')
    subtasks: List[SubTaskSchema] = []
    class Config:
        orm_mode = True



from typing import List, Optional, Any

from sqlalchemy.orm import Session

from crud.base import CRUDBase, ModelType
from app.models.tasks import Task, SubTask
from app.schemas.tasks import TaskSchema, SubTaskSchema, UpdateSubTaskSchema, TaskState
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

class CRUDTask(CRUDBase[Task, TaskSchema, TaskSchema]):
    pass

class CRUDSubTask(CRUDBase[SubTask, SubTaskSchema, UpdateSubTaskSchema]):
    def get(self, db: Session, id: Any) -> Optional[ModelType]:
        return db.query(self.model).filter(self.model.subtask_id == id).first()
    def update_running(self, db: Session, id: Any) -> Optional[ModelType]:
        logging.info("Update the subtask " + str(id) + " to running")
        task_out = self.get(db, id)
        task_in = UpdateSubTaskSchema.from_orm(task_out)
        task_in.state = TaskState.running
        subtask.update(db, db_obj=task_out, obj_in=task_in)
    def update_finished(self, db: Session, id: Any) -> Optional[ModelType]:
        logging.info("Update the subtask " + str(id) + " to finished")
        task_out = self.get(db, id)
        task_in = UpdateSubTaskSchema.from_orm(task_out)
        task_in.state = TaskState.finished
        subtask.update(db, db_obj=task_out, obj_in=task_in)

task = CRUDTask(Task)

subtask = CRUDSubTask(SubTask)

from sqlalchemy.orm import Session
import schemas
import models


def get_data_store(db: Session, ds_id: int):
    return db.query(models.DataStore) \
        .filter(models.DataStore.id == ds_id).first()


def get_data_store_by_name(db: Session, name: str):
    return db.query(models.DataStore) \
        .filter(models.DataStore.name == name).first()


def get_data_stores(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.DataStore).offset(skip).limit(limit).all()


def create_data_store(db: Session, ds: schemas.DataStore):

    db_ds = models.DataStore(
        name=ds.name, user=ds.user, port=ds.port, host=ds.host,
        driver=ds.driver, SPI=ds.SPI, fetch_size=ds.fetch_size,
        max_connections=ds.max_connections, min_connections=ds.min_connections,
        validate_connections=ds.validate_connections, Loose_bbox=ds.Loose_bbox,
        Expose_primary_key=ds.Expose_primary_key,
        Max_open_prepared_statements=ds.Max_open_prepared_statements,
        Estimated_extends=ds.Estimated_extends,
        Connection_timeout=ds.Connection_timeout)

    db.add(db_ds)
    db.commit()
    db.refresh(db_ds)
    return db_ds


def get_indexer_properties(db: Session, ip_id: int):
    return db.query(models.IndexerProperties) \
        .filter(models.IndexerProperties.id == ip_id).first()


def get_indexer_properties_by_name(db: Session, name: str):
    return db.query(models.IndexerProperties) \
        .filter(models.IndexerProperties.Name == name).first()


def get_indexer_properties(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.IndexerProperties).offset(skip).limit(limit).all()


def create_indexer_properties(db: Session, ds: schemas.IndexerProperties):

    db_ip = models.IndexerProperties(
        Name=ds.Name, Cog=ds.Cog, PropertyCollectors=ds.PropertyCollectors,
        TimeAttribute=ds.TimeAttribute, Schema=ds.Schema,
        CanBeEmpty=ds.CanBeEmpty)

    db.add(db_ip)
    db.commit()
    db.refresh(db_ip)
    return db_ip

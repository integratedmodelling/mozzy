from pydantic import BaseModel
from datetime import date, timedelta

class WKTRequest(BaseModel):
    wkt: str

class LandSatRequest(BaseModel):
    wkt: str
    startdate: date = date.today() - timedelta(1)
    enddate: date = date.today()
    projection: str = 'epsg:4326'
    satellite_path_shapefile: str
    pixel_size: float
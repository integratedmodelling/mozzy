from sqlalchemy import Table, Column, Integer, ForeignKey, String, Enum
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
import enum
from database.session import Base
import uuid

class TaskState(str, enum.Enum):
    running = 'running'
    error = 'error'
    finished = 'finished'
    waiting = 'waiting'

class Task(Base):
    __tablename__ = "task"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    description = Column(String(255), index=False)
    state = Column(Enum(TaskState), index=True)
    subtasks = relationship("SubTask")
    class Config:  
        use_enum_values = True 


class SubTask(Base):
    __tablename__ = "subtask"
    id = Column(Integer, primary_key=True, index=True)
    state = Column(Enum(TaskState), index=True)
    subtask_id = Column(String)
    parent_id = Column('parent_id', UUID(as_uuid=True), ForeignKey('task.id'), nullable=False)
    task = relationship("Task", back_populates="subtasks")
    class Config:  
        use_enum_values = True 
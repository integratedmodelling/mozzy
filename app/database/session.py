from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text
from sqlalchemy import exc

import os
import logging
import time
import sys
from config import settings



logging.basicConfig(stream=sys.stdout, level=logging.INFO)

def create_database(instance, name):
    logging.info("Creating Database " + name)
    
    create = "CREATE DATABASE %s ENCODING '%s ' TEMPLATE template0" % (
        name, 'utf-8')

    logging.info("Execute: " + create)
        
    instance.execute(create)

    SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s/%s' % (
        settings.POSTGRES_USER, settings.POSTGRES_PASS, settings.POSTGRES_HOST,
        settings.POSTGRES_DB
    )
    
    ##TODO
    ##Need to create the extensions



def get_engine():
    ##TODO
    ##Need to make this less redudent
    SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s' % (
        settings.POSTGRES_USER, settings.POSTGRES_PASSWORD, settings.POSTGRES_HOST
    )
    
    test_engine = create_engine(SQLALCHEMY_DATABASE_URL, execution_options={
                                'isolation_level': 'AUTOCOMMIT'})
    
    search = "SELECT 1 FROM pg_database WHERE datname='%s'" % settings.POSTGRES_DB
    
    tries = 0

    while tries < 5:
        try:
            test_engine.connect()
            break
        except exc.SQLAlchemyError:
            tries +=1
            logging.info("Database not ready retry in 5 seconds")
            time.sleep(5)

    if tries > 5:
        logging.error("Could not connect to database, program exiting")
        sys.exit()
    
    if len(test_engine.execute(search).fetchall()) != 0:
        SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s/%s' % (
            settings.POSTGRES_USER, settings.POSTGRES_PASSWORD,
            settings.POSTGRES_HOST, settings.POSTGRES_DB
        )
        engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=False)
        logging.info("Connected to database")
        return engine
    
    create_database(test_engine,settings.POSTGRES_DB)

    SQLALCHEMY_DATABASE_URL = 'postgresql://%s:%s@%s/%s' % (
        settings.POSTGRES_USER, settings.POSTGRES_PASSWORD, settings.POSTGRES_HOST,
        settings.POSTGRES_DB
    )
    
    engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=False)
    return engine


engine = get_engine()

SessionLocal = sessionmaker(autocommit=False,
                            autoflush=False,
                            bind=engine)

Base = declarative_base()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
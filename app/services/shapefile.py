from services.base import AppService
import geopandas as gpd
import shutil
import pathlib
import shapely

class ShapefileService(AppService):
    
    def handle_shapefile_upload(self, upload):
        #TODO return some url and created response
        engine = self.db.get_bind()
        file_path = "/tmp/" + upload.filename 
        with open (file_path, 'wb') as buffer:
            shutil.copyfileobj(upload.file, buffer)
        try:
            shape = gpd.read_file(file_path)
            shape.to_postgis(upload.filename.rsplit( ".", 1 )[ 0 ],
                engine, index=True, index_label='Index', if_exists='replace')
            pathlib.Path(file_path).unlink()
        except:
            pathlib.Path(file_path).unlink()
    
    def get_shapefile(self, tablename):
        ##TODO table not found exception
        sql = "SELECT * FROM " + tablename
        engine = self.db.get_bind()
        try:
            df = gpd.GeoDataFrame.from_postgis(sql, engine, geom_col='geometry')
        except ValueError:
            df = gpd.GeoDataFrame.from_postgis(sql, engine, geom_col='geom')
        return df
    
    def get_shapefile_by_wkt_union(self, tablename, wkt):
        shape = shapely.wkt.loads(wkt)
        polygon = gpd.GeoDataFrame(index=[0],geometry=[shape], crs="epsg:4326")
        sql = "SELECT * FROM " + tablename
        engine = self.db.get_bind()
        try:
            df = gpd.GeoDataFrame.from_postgis(sql, engine, geom_col='geometry')
        except ValueError:
            df = gpd.GeoDataFrame.from_postgis(sql, engine, geom_col='geom')
        clipped= (gpd.clip(df, polygon))
        return clipped
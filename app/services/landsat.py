from app.models.tasks import SubTask
from services.base import AppService
from services.shapefile import ShapefileService
from services.tasks import TasksService
from models.requests import LandSatRequest, WKTRequest
import requests
import pandas as pd
import xarray as xr
import os
import rioxarray
import rasterio
from services.minio import minioService
from crud.tasks import task, subtask
from schemas.tasks import TaskSchema, SubTaskSchema, TaskState, UpdateSubTaskSchema
import logging
import sys
from dask.distributed import Client, LocalCluster


logging.basicConfig(stream=sys.stdout, level=logging.INFO)

##TODO
##move this to settings or something like a util
os.environ['GDAL_DISABLE_READDIR_ON_OPEN']='YES'
os.environ['CPL_VSIL_CURL_ALLOWED_EXTENSIONS']='TIF'

class LandSatService():

    def query_cmr_landsat(collection='Landsat_8_OLI_TIRS_C1',
        tier='T1', path=47, row=27):
        ##TODO this should be cahced over in the db and intersected, find some
        ##latest date.  Otherwise we keep smashing and collecting.  Perhaps
        ##it is prudent to play nice.
        data = [f'short_name={collection}',
                f'page_size=2000',
                f'attribute[]=string,CollectionCategory,{tier}',
                f'attribute[]=int,WRSPath,{path}',
                f'attribute[]=int,WRSRow,{row}',
            ]

        query = 'https://cmr.earthdata.nasa.gov/search/granules.json?' + \
            '&'.join(data)

        r = requests.get(query, timeout=100)
        df = pd.DataFrame(r.json()['feed']['entry'])
        return df

    def make_google_archive(pids, bands):
        ##TODO This call can then be changed to handle the making of the db.
        """Turn list of product_ids into pandas dataframe for NDVI analysis."""
        path =  pids[0].split('_')[2][0:3]
        row =  pids[0].split('_')[2][-3:]
        baseurl = f'https://storage.googleapis.com/gcp-public-data-landsat/LC08/01/{path}/{row}'
        dates = [pd.to_datetime(x.split('_')[3]) for x in pids]
        df = pd.DataFrame(dict(product_id=pids, date=dates))
        
        for band in bands:
            df[band] = [f'{baseurl}/{x}/{x}_{band}.TIF' for x in pids]
        
        return df

    def create_multiband_dataset(row, bands=['B4','B5'], chunks={'band': 1, 'x': 2048, 'y': 2048}):
        '''A function to load multiple landsat bands into an xarray dataset '''
        
        # Each image is a dataset containing both band4 and band5
        datasets = []
        for band in bands:
            url = row[band]
            da = xr.open_rasterio(url, chunks=chunks)
            da = da.squeeze().drop(labels='band')
            ds = da.to_dataset(name=band)
            datasets.append(ds)

        DS = xr.merge(datasets)
        
        return DS



class NdviService(AppService):
    def mean_request(self, data, req: LandSatRequest):
        bands= ['B4', 'B5', 'BQA']
        for r in data:
            client = Client("192.168.1.52:8786")
            subtask.update_running(self.db, r[2])

            df = LandSatService.query_cmr_landsat(
                 collection='Landsat_8_OLI_TIRS_C1',tier='T1',
                 path=r[0], row=r[1])
            pids = df.title.tolist()
            ls_dataframe = LandSatService.make_google_archive(pids, bands).query(
                '@req.startdate <= date <= @req.enddate')
            
            datasets = []
            bad_dates= []
            fp = ls_dataframe.iloc[0]['B4']
            # See the profile
            with rasterio.open(fp) as src:
                crs = src.profile['crs']

            for i,row in ls_dataframe.iterrows():
                try:
                    ds = LandSatService.create_multiband_dataset(row, bands)
                    datasets.append(ds)
                except Exception as e:
                    bad_dates.append(row.date)

            df_clean = ls_dataframe.query('date not in @bad_dates')
            logging.info("Preparing data to be loaded for subtask: " + r[2])
            DS = xr.concat(datasets, dim=pd.DatetimeIndex(df_clean.date.tolist(), name='time'))
            logging.info('Dataset size (Gb): ' + str(DS.nbytes/1e9) + "for subtask " + r[2])
            DS['CLOUD']= DS['BQA'].isin([2720,2724,2728,2732])
            DS['CLOUD']=DS['CLOUD'].where(DS['CLOUD'] !=0)
            DS['B5']=DS['B5'].where(DS['B5'] !=0)
            DS['B4']=DS['B4'].where(DS['B4'] !=0)
            NDVI = ((DS['B5'] - DS['B4']) / (DS['B5'] + DS['B4'])) * DS['CLOUD']
            ##ndvi = NDVI.sel(time=slice(req.startdate, req.enddate)).mean(dim='time').compute()
            ndvi = NDVI.mean(dim='time').load()
            ndvi=(ndvi*1000)
            ndvi=ndvi.where(ndvi > 0)
            ndvi=ndvi.fillna(-9999)
            ndvi=ndvi.astype('int16')
            ndvi.rio.write_nodata(-9999, inplace=True)
            ndvi.rio.set_crs(crs)
            ndvi.rio.write_crs("epsg:4326")
            path = "/tmp/" + r[2].split("_")[0] + "/" + r[3]
            ndvi.rio.to_raster(path, compress='deflate')

            minioService.upload(bucket_path=  r[2].split("_")[0] + "/" + r[3], file_path = path)
            subtask.update_finished(self.db, r[2])
            client.restart()
            
    
    def process_request(self, request: LandSatRequest, operation: str ):
        task_id = str(task.create(self.db, obj_in=TaskSchema(state="running")).id)
        path_row = ShapefileService(self.db).get_shapefile_by_wkt_union(
            request.satellite_path_shapefile, request.wkt)[["PATH", "ROW"]]
        
        investigate= []
        filelist = []

        filelist.append(task_id + ".tif")

        for r in path_row.iterrows():
            path = r[1]['PATH']
            row = r[1]['ROW']
            id = task_id + "_" + str(path).zfill(3) + str(row).zfill(3)
            subtask_file = id + ".tif"
            interest=[path,  row, id, subtask_file]
            filelist.append(subtask_file)
            investigate.append(interest)
            subtask.create(self.db,
                obj_in=SubTaskSchema(state="waiting", parent_id=task_id,
                    subtask_id=id))
        
        os.makedirs("/tmp/" + task_id)

        with open('/tmp/' + task_id + 'filelist.txt', 'w') as filehandle:
            filehandle.writelines("%s\n" % file for file in filelist)

        minioService.upload(bucket_path=task_id + "/" + "filelist.txt",
            file_path= "/tmp/" + task_id + "filelist.txt")

        return  TaskSchema.from_orm(task.get(self.db, task_id)), investigate

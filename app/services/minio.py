from minio import Minio
from minio.commonconfig import Tags, GOVERNANCE
from minio.error import S3Error

from config import settings
import logging
import sys
from time import sleep
from datetime import datetime, timedelta

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

class MinioService():
    def __init__(self):
        logging.info("Setting up minio")
        retries = 10
        while (retries > 0):
            try:
                self.client = Minio(
                    endpoint = settings.MINIO_URL,
                    access_key = settings.MINIO_USER,
                    secret_key = settings.MINIO_PASSWORD,
                    secure = settings.MINIO_SECURE,
                    region = settings.MINIO_REGION_NAME
                )

                self.bucket = settings.MINIO_BUCKET
                self.setup()
                retries = 0
            except:
                retries -= 1
                sleep(5)

    def setup(self):
        logging.info("Setting up minio client for : " + settings.MINIO_URL)
        found = self.client.bucket_exists(self.bucket)
        if not found:
            logging.info("Making bucket for storage at " + settings.MINIO_BUCKET)
            self.client.make_bucket(bucket_name=settings.MINIO_BUCKET,
                location= settings.MINIO_REGION_NAME)
        else:
            logging.info("Bucket already at " + settings.MINIO_BUCKET)

    def upload(self, bucket_path: str, file_path: str, **tags):
        if tags:
            tags = Tags(for_object=True)
            for k,v in tags:
                tags[k] = v
            result = self.client.fput_object(
                self.bucket, bucket_path, file_path, tags=tags
            )
            return result
        else:
            result = self.client.fput_object(
                self.bucket, bucket_path, file_path,
            )
            return result


minioService = MinioService()
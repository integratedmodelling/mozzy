from services.base import AppService
from crud.tasks import task
from sqlalchemy.orm import Session

class TasksService(AppService):
    def __init__(self, db: Session):
        self.task = task
        super().__init__(db)
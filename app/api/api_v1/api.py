from fastapi import APIRouter

from app.api.api_v1.endpoints import processes, shapefiles, tasks
api_router = APIRouter()
api_router.include_router(shapefiles.router, prefix="/shapefiles")
api_router.include_router(processes.router, prefix="/processes")
api_router.include_router(tasks.router, prefix="/tasks")
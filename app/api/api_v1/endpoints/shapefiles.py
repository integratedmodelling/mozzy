from fastapi import APIRouter, File, UploadFile,HTTPException, Depends, Query
from services.shapefile import ShapefileService
from database.session import get_db
from models.requests import WKTRequest
from typing import Optional
import re

router = APIRouter()

filename_pattern = re.compile("[a-z0-9_]+")

@router.post("/uploadfile/")
async def create_upload_file(upload: UploadFile = File(...),
    db: get_db = Depends()):
    if upload.content_type == "application/zip":
        if filename_pattern.fullmatch(upload.filename.rsplit( ".", 1 )[ 0 ]):
            ShapefileService(db).handle_shapefile_upload(upload)
        else:
            raise HTTPException(status_code=422,
                detail="File name must only container the following character: " +
                    "a-z 0-9 and _")
    else:
         raise HTTPException(status_code=422,
            detail="File not recognized as a shapefile")


@router.get("/{shapefile_id}")
async def get_shapefile(shapefile_id: str, db: get_db = Depends()):
    return ShapefileService(db).get_shapefile(shapefile_id).to_json()

@router.post("/{shapefile_id}")
async def get_shapefile_union_wkt(shapefile_id: str, 
    wkt : WKTRequest, db: get_db = Depends(),
    operation: str = Query("union", enum=["union"])):
    if operation == "union":
        return ShapefileService(db).get_shapefile_by_wkt_union(
            shapefile_id,wkt.wkt).to_json()
from fastapi import APIRouter,  Depends

from database.session import get_db
from schemas.tasks import TaskSchema
from crud.tasks import task


router = APIRouter()

@router.post("/", response_model=TaskSchema)
def post_task(obj: TaskSchema, db: get_db = Depends()):
    return task.create(db, obj_in=obj)
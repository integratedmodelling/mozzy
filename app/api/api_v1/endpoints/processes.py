from fastapi import APIRouter,  HTTPException, Depends, Query, BackgroundTasks
from services.landsat import NdviService
from models.requests import LandSatRequest
from database.session import get_db

router = APIRouter()

@router.post("/landsat-ndvi")
async def create_landsat_ndvi(background_tasks: BackgroundTasks,
    request: LandSatRequest,
    db: get_db = Depends(),
    operation: str = Query("mean", enum=["mean", "max", "min"])
):
    response, interest = NdviService(db).process_request(request,operation)
    background_tasks.add_task(NdviService(db).mean_request, interest, request)
    return response
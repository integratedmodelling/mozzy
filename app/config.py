from pydantic import BaseSettings

class Settings(BaseSettings):
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_HOST: str
    POSTGRES_DB: str
    MINIO_USER: str
    MINIO_PASSWORD: str
    MINIO_URL: str
    MINIO_BUCKET = 'mozzy'
    MINIO_SECURE = False
    MINIO_REGION_NAME = "eu-im-1"

settings = Settings()